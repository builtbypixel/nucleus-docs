export default {
  breakpoints: ["30em", "48em", "62em", "80em"],
  fonts: {
    body: "Oxygen",
    title: "Inter",
    main: "Oxygen",
  },

  colors: {
    sky: "#cdecff",
    feint: "#d3d3d3",
    main: "#0058e6",
    orangeButton: "#FA674D",
    clear: "transparent",
  },
};
