import { Box, Text, Button, ThemeProvider, Flex } from "@builtbypixel/plasma";
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import GettingStarted from "./components/GettingStarted";
import About from "./components/About";
import Home from "./components/Home";
import Containers from "./components/ContainersHome";
import Nav from "./components/Nav";

function App() {
  return (
    <ThemeProvider>
      <Box>
        <Flex bg="#0058e6" height="50px" position="fixed" width="100%">
          <Text mx="auto" fontSize="24px" my="8px" color="white">
            Nucleus docs
          </Text>
        </Flex>
        <Nav />
      </Box>
    </ThemeProvider>
  );
}

export default App;
