import React from "react";
import { Box, Text, Flex, Image } from "@builtbypixel/plasma";
import Code from "./styles/Code";
import PageContainer from "./PageContainer";
import Title from "./styles/Title";
import Subtitle from "./styles/Subtitle";

const Links = () => {
  return (
    <PageContainer>
      <Box>
        <Title>Links</Title>
        <Text>
          In <Code>/src</Code> there is a file called <Code>links.js</Code>.
          This file creates a sidebar in nucleus that contains links to all of
          the containers. You will need to provide a link to all the containers
          you make in this file. To add a link to a container, create an object
          in the <Code>menuLinks</Code> array. In this object you will need to
          give two properties: <Code>group</Code> and <Code>link</Code>. Set the
          value of <Code>group</Code> to whatever you would like the label to be
          in the menu. The value of <Code>link</Code> needs to be the path to
          that container. Usually this will be just the model name (set in
          setup.js) which will take the user to the list view for that container
          by default e.g. <Code>/products</Code>, but the link can be to
          anywhere you want. It can go to the edit view or the create view for
          example. You just need to provide the correct internal link e.g.{" "}
          <Code>/products/create</Code>. You will also need to add a{" "}
          <Code>items</Code> property with the value of an empty array. See
          below for an example
        </Text>
        <Image src="images/links.png" />

        <Subtitle>Submenus</Subtitle>
        <Text>
          You can also create submenus to help organise these links. For
          example, if you have several containers for pages, you can group the
          links to each of these under a <Code>pages</Code> submenu.
          <br />
          In order to create a submenu, create an object in{" "}
          <Code>menuLinks</Code> as normal but this time only give it a{" "}
          <Code>group</Code> property and leave out the <Code>link</Code>{" "}
          property. In the <Code>items</Code> property, create an object with
          the following properties: <Code>title</Code> and <Code>href</Code>.
          Give title the value of the label that you want for the link and href
          the value of the link itself. See below for an example of the code and
          how this will appear in nucleus.
        </Text>
        <Flex>
          <Box width="50%">
            <Image src="images/submenu.png" />
          </Box>
          <Box width="30%">
            <Image src="images/submenu_nucleus.png" />
          </Box>
        </Flex>
      </Box>
    </PageContainer>
  );
};

export default Links;
