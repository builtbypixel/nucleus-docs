import React from "react";
import { Box, Flex, Text, Image } from "@builtbypixel/plasma";
import Code from "./styles/Code";
import PageContainer from "./PageContainer";
import Title from "./styles/Title";
import Subtitle from "./styles/Subtitle";

const Setup = () => {
  return (
    <PageContainer>
      <Box>
        <Title>Setup.js</Title>
        <Text>
          This is where you provide the endpoint, and determine what the user
          can and can't do with the data.
          <br />
          Here is what a typical setup file looks like:
          <Image src="images/setup_file.png" />
          Here is what each part of the setup file does:
        </Text>
        <Text fontWeight="bold">modelName</Text>
        <Text>
          This determines what the url for this container will be. So if you set
          the modelName to <Code>User details</Code>, the function in{" "}
          <Code>model</Code> will convert it to <Code>user-details</Code> which
          will be the endpoint that will point to that container. This is why in
          the links, you must set the href to the model ( which in this case
          would be <Code>user-details</Code>) the modelName should usually be
          plural and it's a good idea to give it the same name as the endpoint
          although you don't have to.
        </Text>
        <Text fontWeight="bold">modelNameSingular</Text>
        <Text>
          This is the same as the model name but singular instead of plural it
          will be singular. So for the <Code>/products</Code> endpoint, the
          modelNameSingular will be 'Product'. This is needed so that if a user
          adds, or edits a single endpoint, nucleus will show 'edit Product' or
          'add new Product'.
        </Text>
        <Text fontWeight="bold">Endpoint</Text>
        <Text>
          This is the endpoint that this container controls. Once you provide an
          endpoint, nucleus will combine this with the api you provided in
          <Code>.env</Code> to create the POST, PUT and DELETE requests. For
          example, if you have <Code>REACT_APP_API=https://endpoint.co.uk</Code>{" "}
          in your <Code>.env</Code> file and you have{" "}
          <Code>endpoint = "/products"</Code>
          in your <Code>setup.js</Code> file, then this container will POST, PUT
          etc to https://endpoint.co.uk/products.
        </Text>
        <Text fontWeight="bold">canDelete</Text>
        <Text>
          if set to true, the user will be able to delete an entry. So for
          <Code>/products</Code> for example, the user will be able to delete
          one or more products using nucleus
        </Text>
        <Text fontWeight="bold">canAddNew</Text>
        <Text>
          if set to true, the user will be able to add an entry. If this is
          true, a button will show up in the edit view allowing users to enter
          in a form to add a new entry to the data
        </Text>
        <Text fontWeight="bold">canSearch</Text>
        <Text>
          If set to true there will be a search bar that renders in the list
          view.
        </Text>
        <Text>
          You do not need to edit, title, singular, model, endpoint or accessor
        </Text>
      </Box>
    </PageContainer>
  );
};

export default Setup;
