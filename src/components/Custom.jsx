import React from "react";
import { Flex, Box, Text, Image } from "@builtbypixel/plasma";
import PageContainer from "./PageContainer";
import Title from "./styles/Title";
import Code from "./styles/Code";
import Subtitle from "./styles/Subtitle";

const Custom = () => {
  return (
    <PageContainer>
      <Title>Custom Components</Title>
      <Box>
        <Text>
          If you need a greater level of control than using either the{" "}
          <Code>&#60;Field/&#62;</Code> or auto fields, you need to create and
          use <b>custom components</b>.
        </Text>
        <Text mt="15px">
          Custom components are react components that you can build youself that
          uses the <b>form context</b> from react hook forms to get, edit and
          save the data. In other words, you will need to use the tools from
          react hook forms directly. Nucleus won't be doing anything under the
          hood to help you.
        </Text>
        <Text mt="15px">
          To get started, you will need to create a functional component. Below
          shows a simple example of this.
        </Text>
        <Image src="/images/basic_custom.png" />
        <Text>
          As you can see, this is just a very simple functional component. After
          you've made your component, you have associate it with a field in
          nucleus which you can do for both auto fields, or manual fields. To do
          this, import the component into the index.jsx file, then pass it to
          the component key or prop. See below for an example of how this should
          look.
        </Text>
        <Flex justifyContent="space-between" mt="10px">
          <Box>
            <Subtitle>Autofields object</Subtitle>
            <Image src="/images/auto_custom.png" />
          </Box>
          <Box>
            <Subtitle>Manual field component</Subtitle>
            <Image src="/images/manual_custom.png" />
          </Box>
        </Flex>
        <Text mt="15px">
          As you can see above, all you really need for the field is the name
          and the component itself. You don't need to add a placeholder, label,
          rules etc. That is the whole point of custom components! You can
          display and save any data that you want. Now lets look at a few use
          cases for custom components and explore what you can do.
        </Text>
        <Subtitle>Read only data</Subtitle>
        <Text>
          Sometimes there will be some data that you don't want the user to be
          able to edit. For example, maybe in the user data, there is a field
          called communication preferences, but this is something that only the
          user themselves should be able to control on the frontend and not for
          the admin on nucleus.
        </Text>
        <Text mt="15px">
          One way of dealing with this is to set <Code>disabled: true</Code> on
          the field so the user won't be able to edit it. This will work but it
          can come across as misleading to the user as it may look as if they
          are locked out or that nucleus isn't working as it should. If we use a
          custom component, we can grab the data from the form, display it any
          way we'd like and it will render just like a normal field.
        </Text>
        <Text mt="15px">
          One way of dealing with this is to set <Code>disabled: true</Code> on
          the field so the user won't be able to edit it. This will work but it
          can come across as misleading to the user as it may look as if they
          are locked out or that nucleus isn't working as it should. If we use a
          custom component, we can grab the data from the form, display it any
          way we'd like and it will render just like a normal field. To do this
          we first need to get the data from that field from the props. When you
          use a custom component in nucleus, an object is automatically passed
          in the props which contains, the key, the data and some other useful
          functions. All we need to do is access the data from the props, and
          display it in a text field.
        </Text>
        <Image src="/images/custom_simple.png" my="20px" />
        <Text>
          Here you can see that the value is accessible on the props object and
          can be rendered into a simple text component. <b>Remember-</b> You
          don't have to pass props anywhere to the custom component. Nucleus
          will do this for you. `
        </Text>
        <Subtitle>Getting other data</Subtitle>
        <Text>
          If you need to get more data other than just the data for the field
          you are using, you can use the react hook form function{" "}
          <Code>getValues</Code>. You can destructure getValues from
          <Code>useFormContext</Code>. Once you invoke getValues it will return
          all of the data that is available for that container.
        </Text>
        <Image src="/images/getValues.png" my="10px" />
        <Text>
          In the custom component above, we have used <Code>getValues</Code> to
          get all the values in the form data for that data and display them.
          It's important to note that getValues does <b>not</b> update once the
          values change and does <b>not</b> subscribe to any changes in the
          data. If you need to get the values as they change, use{" "}
          <Code>useWatch</Code> instead.
        </Text>
        <Subtitle>Saving data to the form</Subtitle>
        <Text>
          Sometimes in nucleus, we want control what information is <b>saved</b>{" "}
          and not just get the data back from the form. You can do this in
          custom components by using <Code>setValue</Code>.{" "}
          <Code>setValue</Code> is an easy function to use. First you have to
          extract it from the form context like getValues. Then all you need to
          do is invoke it with two arguments: the key, and the value that you
          want to set. For example, if I wanted to make sure that the value for
          "name" is set to "Bill" every time, I can use a custom component for
          that field that looks like this:
        </Text>
        <Image src="/images/setvalue-example.png" />
        <Subtitle>Dynamically setting data</Subtitle>
        <Text>
          One of the main advatages of using custom components is you can modify
          the data before you save it. For one example of how this could be
          used, we will look at Barn Direct nucleus. Here, for the edit view in
          products, there needs to be a slug for each product that is created or
          edited. We could allow the user to enter their own slug but it is very
          unlikely that the client will know what a slug is and that it can't
          have spaces etc. What would be more useful is if we could generate a
          slug for them based on the name for that product. So if the client
          added a product with the name "Red wine" we could automatically
          generate a slug of "red-wine". Let's go through step by step how we
          could create a custom component to do this.
        </Text>
        <Text mt="15px">
          First, we need to create a simple functional component and assign it
          to the component key for the field we want to edit (in this case the
          slug field). I have called the custom component "SlugGenerator"
        </Text>
        <Image src="/images/slug1.png" />
        <Text mt="15px">
          In our custom component, we know we will need <Code>setValue</Code> so
          we can destructure that from <Code>useFormContext</Code>. Since the
          slug we want to generate will depend upon the name, we need something
          to watch the value for name and update each time the value changes. We
          can do that with <Code>useWatch</Code> that will update to any of the
          changes that the user makes to the name. Here I have set the value of
          useWatch.name to propName and also used the nullish operator{" "}
          <Code>??</Code> to check if it's undefined and if it is, assign it to
          an empty string. This will help reduce errors as when the client is
          creating a new product, the value for name will be undefined
          initially.
        </Text>
        <Image src="/images/slug2.png" />
        <Text mt="15px">
          Next, we need to use a <Code>useEffect</Code> to run every time the
          name changes, and a <Code>useState</Code> to store the slug in state
          once we have generated it.
        </Text>
        <Image src="/images/slug3.png" />
        <Text mt="15px">
          Now we can use a <Code>.replace()</Code> to generate the slug for us
          in the use effect, set it to state and finally save it in the form
          data using <Code>setValue</Code>
        </Text>
        <Image src="/images/slug4.png" />
        <Text mt="15px">
          Finally, we can display the data as the value in an input component to
          make the style match with the other fields in the form.
        </Text>
        <Image src="/images/slug5.png" />
      </Box>
    </PageContainer>
  );
};

export default Custom;
