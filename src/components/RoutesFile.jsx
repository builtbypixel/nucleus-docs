import React from "react";
import { Box, Flex, Text, Image } from "@builtbypixel/plasma";
import Code from "./styles/Code";
import PageContainer from "./PageContainer";
import Title from "./styles/Title";
import Subtitle from "./styles/Subtitle";

const RoutesFile = () => {
  return (
    <PageContainer>
      <Box>
        <Title>routes.js</Title>
        <Subtitle>Views</Subtitle>
        <Text>
          The routes file organises different components for the container. In
          each container, there are four different views: edit view, and list
          view, view view and create view. <br />
          <b>Edit</b> view, allows users to edit an entry from the endpoint. In
          the edit view, you will provide the users with a form to edit the data
          for a particular entry. For example, for the endpoint{" "}
          <Code>/venues</Code>, the edit view will render a form for a
          particular venue. This form will contain fields based on the data
          structure of the JSON response from <Code>/venues</Code>.<br />
          <b>List</b> view is used to create a small summary table for the data.
          Not all the data needs to be included in the list view as this is
          mainly used as a reference and also to link to the edit view. Users
          should not be able to edit any of the data in this view. They should
          only be able to view, sort and search the data.
          <br />
          <b>Create</b> view comes comes with edit view and allows users to
          create new entries for the endpoint. Create view uses the same form as
          the one in edit view but instead of creating a PUT request, it creates
          a POST request to the backend.
          <br />
          <b>View</b> view, as the name suggests, is only used for viewing the
          data. This view should only be used if the user needs to view all of
          the details of an entry. For example, if the list view for a{" "}
          <Code>/qualifications</Code> endpoint only shows some of the data
          (like awarding body and level), then it could be a good idea to
          provide a link to a view view where the user can see all the details
          of each qualification (like modules, assessments, resources etc) where
          the users can see all the information but you don't want the user to
          be able to edit the data.
          <Box width="40%">
            <Image src="images/routes.png" />
          </Box>
          <Text>
            Here is the routes file for the nucleus boiler plate. For most cases
            this doesn't need to be edited at all but you can change this file
            depending on what you need. In the code, you will notice a function
            called <Code>createCrudRoutes</Code>. This is always given an object
            that will match each nucleus view with each component that you
            provide. Below explains how this object needs to be set up.
          </Text>
          <Subtitle>createCrudRoutes</Subtitle>
          <Text>
            <Code>setup</Code>
          </Text>
          <Text>
            This is the whole of the setup object which has been imported from{" "}
            <Code>setup.js</Code>
          </Text>
          <Text>
            <Code>name</Code>
          </Text>
          <Text>
            In the boilerplate, this is the title that has been entered in the
            setup file (e.g. Users, Products). Like most of
            <Code>routes.js</Code>, this doesn't need to be changed.
          </Text>
          <Text>
            <Code>edit</Code>
          </Text>
          <Text>
            This is where you need to put the edit view component. By default
            the edit view (called Entry) is already provided and is imported
            from <Code>'./'</Code> (which is index.js for this folder).
          </Text>
          <Text>
            <Code>list</Code>
          </Text>
          <Text>
            This is where you put the list view component for the enpoint. In
            the boilerplate, this is already set to <Code>List</Code> which is
            imported from <Code>List.jsx</Code>
          </Text>
          <Text>
            <Code>view</Code>
          </Text>
          <Text>
            This is where you put the view view component for the enpoint. This
            is the only component not provided in the boilerplate. If you need
            to use a view component, create the file, import it into the routes
            file and set it as the value of <Code>view:</Code>
          </Text>
        </Text>
        <Subtitle>createPageRoutes</Subtitle>
        <Text>
          There are some cases where you will need different forms to edit data
          from the same endpoint. One of the most common examples of this is
          page data. These will all come from the same endpoint (usually{" "}
          <Code>/pages</Code>), but depending on the page, the data will be
          different. To accomodate for this, you will need to use another
          nucleus function called <Code>createPageRoutes</Code>. By using this,
          you can create a separate edit view for each page. If you are using
          this function, you will need to create an edit view for each page in
          the container. The easiest way to do this is by creating a new folder
          for each page and creating an <Code>index.jsx</Code> file inside that
          folder. see below for an example of how this should look.
        </Text>
        <Image src="/images/pages.png" />
        <Text>
          Now in your <Code>routes.js</Code> file you will need to create an
          array of objects called pages. In each of these objects, you will need
          to provide the id number of the particular page, the component for the
          edit view which you will need to import, and a title. See below for an
          example.
        </Text>
        <Text fontStyle="italic">
          <b>Note</b>- in this example the id is a hash because it is using a
          mongoDB database
        </Text>
        <Image src="/images/createPageRoutes.png" />
        <Text>
          Then all you have to do is change the <Code>crud</Code> so that it
          uses the <Code>createPageRoutes</Code> function instead of{" "}
          <Code>createCrudRoutes</Code> and pass in the list view, the pages
          array and the setup file
        </Text>
      </Box>
    </PageContainer>
  );
};

export default RoutesFile;
