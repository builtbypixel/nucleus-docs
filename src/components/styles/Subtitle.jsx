import React from "react";
import { Flex, Box, Text } from "@builtbypixel/plasma";

const Subtitle = ({ children, width }) => {
  return (
    <Flex
      bg="#0058e6"
      width={width ?? "200px"}
      rounded="full"
      justifyContent="center"
      color="white"
      my="8px"
      p="3px"
    >
      <Box>
        <Text>{children}</Text>
      </Box>
    </Flex>
  );
};

export default Subtitle;
