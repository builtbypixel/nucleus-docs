import React from "react";
import styled from "styled-components";

const Code = styled.code`
  background-color: #62efff;
`;

export default Code;
