import React from "react";
import { Box, Flex, Text } from "@builtbypixel/plasma";

const Title = ({ children, mt }) => {
  return (
    <Flex
      bg="#0058e6"
      textAlign="center"
      rounded="full"
      height="30px"
      alignItems="center"
      color="white"
      fontSize="20px"
      mb="10px"
      mt={mt ?? "0px"}
    >
      <Text flexBasis="100%">{children}</Text>
    </Flex>
  );
};

export default Title;
