import React from "react";
import { Box, Flex, Text } from "@builtbypixel/plasma";
import Setup from "./Setup";
import RoutesFile from "./RoutesFile";
import List from "./List";
import Edit from "./Edit";
import PageContainer from "./PageContainer";
import Code from "./styles/Code";
import Title from "./styles/Title";

const ContainersHome = () => {
  return (
    <PageContainer>
      <Title>Containers</Title>
      <Text>
        In nucleus, you can think of each container as responsible for control
        one endpoint each. For example, you might have a container for the{" "}
        <Code>/users</Code> endpoint. In that container, you can setup CRUD
        (create, read, update and delete) routes for that endpoint. In the
        nucleus boiler plate, you should already have an example container in{" "}
        <Code>/src/containers/example</Code>. To make a new container, you can
        copy and paste the example folder into <Code>src/containers</Code> and
        rename the folder.
      </Text>
      <Text>
        In each container, you will notice there are always four files:
        <br />
        setup.js, routes.js, index.jsx and list.jsx
      </Text>
      <Text>
        Click the links to read about what each of these files do and how to
        configure them.
      </Text>
    </PageContainer>
  );
};

export default ContainersHome;
