import React from "react";
import {
  Flex,
  Box,
  Text,
  Button,
  Accordion,
  AccordionButton,
  AccordionItem,
  AccordionPanel,
} from "@builtbypixel/plasma";
import { BrowserRouter as Router, Link, Switch, Route } from "react-router-dom";
import GettingStarted from "./GettingStarted";
import About from "./About";
import Home from "./Home";
import Containers from "./ContainersHome";
import Setup from "./Setup";
import List from "./List";
import Edit from "./Edit";
import Routes from "./RoutesFile";
import MainRouter from "./MainRouter";
import Links from "./Links";
import Manual from "./Manual";
import Custom from "./Custom";
import GqlIntro from "./gqlIntro";
import GqlList from "./gqlList";
import GqlEdit from "./gqledit";

const Nav = () => {
  return (
    <Box>
      <Router>
        <Flex>
          <Box
            overflow="scroll"
            bg="#cdecff"
            height="100vh"
            px="3px"
            position="fixed"
            width="150px"
            mt="50px"
          >
            <Link to="/">
              <Button
                display="block"
                width="100%"
                my="10px"
                border="1px solid black"
              >
                Home
              </Button>
            </Link>
            <Link to="/gettingstarted">
              <Button
                display="block"
                width="100%"
                my="10px"
                border="1px solid black"
              >
                Getting Started
              </Button>
            </Link>
            <Link to="/about">
              <Button
                display="block"
                width="100%"
                my="10px"
                border="1px solid black"
              >
                About
              </Button>
            </Link>
            <Accordion
              display="block"
              width="100%"
              my="10px"
              border="1px solid black"
              collapsible={true}
            >
              <AccordionItem>
                <Button
                  display="block"
                  width="100%"
                  my="10px"
                  border="1px solid black"
                >
                  <AccordionButton arrowAlign="right">
                    <Link to="/containers">Containers</Link>
                  </AccordionButton>
                </Button>
                <AccordionPanel>
                  <Link to="/setup">
                    <Button
                      display="block"
                      width="100%"
                      my="10px"
                      border="1px solid black"
                    >
                      Setup.js
                    </Button>
                  </Link>
                  <Link to="/list">
                    <Button
                      display="block"
                      width="100%"
                      my="10px"
                      border="1px solid black"
                    >
                      List.jsx
                    </Button>
                  </Link>
                  <Link to="/edit">
                    <Button
                      display="block"
                      width="100%"
                      my="10px"
                      border="1px solid black"
                    >
                      index.jsx
                    </Button>
                  </Link>
                  <Link to="/routes">
                    <Button
                      display="block"
                      width="100%"
                      my="10px"
                      border="1px solid black"
                    >
                      routes.js
                    </Button>
                  </Link>
                </AccordionPanel>
              </AccordionItem>
            </Accordion>
            <Link to="/router">
              <Button
                display="block"
                width="100%"
                my="10px"
                border="1px solid black"
              >
                Router
              </Button>
            </Link>
            <Link to="/links">
              <Button
                display="block"
                width="100%"
                my="10px"
                border="1px solid black"
              >
                Links
              </Button>
            </Link>
            <Text textAlign="center">Advanced</Text>
            <Link to="/manual">
              <Button
                display="block"
                width="100%"
                my="10px"
                border="1px solid black"
              >
                Manual Fields
              </Button>
            </Link>
            <Link to="/custom">
              <Button
                display="block"
                width="100%"
                my="10px"
                border="1px solid black"
                height="50px"
              >
                Custom <br />
                components
              </Button>
            </Link>
            <Text textAlign="center">GraphQL</Text>
            <Link to="/gqlintro">
              <Button
                display="block"
                width="100%"
                my="10px"
                border="1px solid black"
              >
                Intro
              </Button>
            </Link>
            <Link to="/gqllist">
              <Button
                display="block"
                width="100%"
                my="10px"
                border="1px solid black"
              >
                List
              </Button>
            </Link>
            <Link to="/gqledit">
              <Button
                display="block"
                width="100%"
                my="10px"
                border="1px solid black"
              >
                Edit
              </Button>
            </Link>
          </Box>
          <Box ml="150px" mt="50px">
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route path="/gettingstarted">
                <GettingStarted />
              </Route>
              <Route path="/about">
                <About />
              </Route>
              <Route path="/containers">
                <Containers />
              </Route>
              <Route path="/setup">
                <Setup />
              </Route>
              <Route path="/list">
                <List />
              </Route>
              <Route path="/edit">
                <Edit />
              </Route>
              <Route path="/routes">
                <Routes />
              </Route>
              <Route path="/router">
                <MainRouter />
              </Route>
              <Route path="/links">
                <Links />
              </Route>
              <Route path="/manual">
                <Manual />
              </Route>
              <Route path="/custom">
                <Custom />
              </Route>
              <Route path="/gqlintro">
                <GqlIntro />
              </Route>
              <Route path="/gqllist">
                <GqlList />
              </Route>
              <Route path="/gqledit">
                <GqlEdit />
              </Route>
            </Switch>
          </Box>
        </Flex>
      </Router>
    </Box>
  );
};

export default Nav;
