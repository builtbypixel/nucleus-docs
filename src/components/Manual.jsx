import React from "react";
import { Box, Flex, Text, Image, Grid } from "@builtbypixel/plasma";
import Code from "./styles/Code";
import PageContainer from "./PageContainer";
import Title from "./styles/Title";
import Subtitle from "./styles/Subtitle";

const Manual = () => {
  return (
    <PageContainer>
      <Title>Manual Fields</Title>
      <Text>
        Manual fields works in a very similar way to auto fields but with much
        greater flexibility. Instead of using the{" "}
        <Code>&#60;AutoFields&#47;&#62;</Code> component, you use the{" "}
        <Code>&#60;Field&#47;&#62;</Code> components (which are imported from
        nucleus)
      </Text>
      <Text>
        Let's look at a simple example for running imp in the edit view for the
        Departments container. Here you can see two examples of how to code the
        same container and see how the are different. Note that this is the edit
        view of the component.
      </Text>
      <Grid templateColumns={"1fr 1fr"} my="20px" gap="5px">
        <Text fontSize="28px" gridColumn="span 2" textAlign="center">
          Auto Fields
        </Text>
        <Box>
          <Image src="/images/auto_code_example.png" />
          <Text>^ Using autofields in VS code</Text>
        </Box>
        <Box>
          <Image src="/images/auto_example.png" />
          <Text>^ How it looks in nucleus</Text>
        </Box>
        <Text fontSize="28px" gridColumn="span 2" textAlign="center" mt="30px">
          Manual Fields
        </Text>
        <Box>
          <Image src="/images/Manual_code_example.png" />
          <Text>^ Using autofields in VS code</Text>
        </Box>
        <Box>
          <Image src="/images/Manual_example.png" />
          <Text>^ How it looks in nucleus</Text>
        </Box>
      </Grid>
      <Text>
        We know that using autofields, you create an array of field objects each
        divided into groups which is then passed into the{" "}
        <Code>&#60;AutoFields/&#62;</Code> component.
      </Text>
      <Text>
        If we use manual fields, we don't need to create a fields object or use
        the <Code>&#60;AutoFields/&#62;</Code> component. Instead, we use the
        simple <Code>&#60;Field/&#62;</Code> component imported from nucleus to
        make fields which are children of the <Code>&#60;EditView/&#62;</Code>{" "}
        component. Using manual fields is very similar to using auto fields. If
        you look at the example above, the makeup of the fields are very
        similar. You still need a name, label and component type. The only
        difference is with auto fields you are creating objects which nucleus
        will use to make a field (so your syntax must be valid JSON). With
        manual fields, you are adding props to the <Code>&#60;Field/&#62;</Code>{" "}
        component.
      </Text>
      <Text mt="15px">
        One of the main advantages of using manual fields is the amount of
        control it gives you on the front end. You can add the fields and
        position them however you want. You can add subtitles or sections of
        text wherever you want. Anything you can do with a normal react
        frontend, you can do in nucleus. As long as they are children of{" "}
        <Code>&#60;EditView/&#62;</Code>, they will be rendered along with the
        rest of the fields.
      </Text>
      <Text mt="15px">
        If you are using autofields, you have almost no control in how these
        fields are rendered in nucleus. It is all done for you in the{" "}
        <Code>&#60;AutoFields/&#62;</Code> component.
      </Text>
    </PageContainer>
  );
};

export default Manual;
