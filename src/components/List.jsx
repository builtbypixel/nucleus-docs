import React from "react";
import { Box, Flex, Text, Image } from "@builtbypixel/plasma";
import Code from "./styles/Code";
import PageContainer from "./PageContainer";
import Title from "./styles/Title";
import Subtitle from "./styles/Subtitle";

const List = () => {
  return (
    <PageContainer>
      <Box>
        <Title>List.jsx</Title>
        <Text>
          The List view component provides the user with a small summary table
          showing the data from an endpoint. It doesn't have to show all the
          data and it isn't possible to edit or create new entries in this view
          although you can access both the create and edit view from here. The
          list view also allows users to sort and search for particular entries
          although this does need backend support to work.
        </Text>
        <Text>
          In the list view there are certain components that are already
          rendered out of the box. You have a search bar, you can click the
          headers of the table to sort, you can click 'Create New' to create a
          new entry, you can change the pagination and if you select entries,
          you can delete them. Remember that these are the features that are
          specified in setup.js e.g. <Code>canDelete: true</Code>
        </Text>
        <Text>
          Before we look at what is in the boilerplate, here is a simplified
          example of a list view from Parfetts Corporate. and what this will
          render in nucleus
        </Text>
        <Flex>
          <Box width="100%">
            <Image src="images/simple_list.png" />
          </Box>
          <Box>
            <Image src="images/list_view_1.png" />
          </Box>
        </Flex>
        <Subtitle> Columns </Subtitle>
        <Text>
          The only part of the list view you need to worry about is the{" "}
          <Code>columns</Code> array (line 13). In the columns array, each
          object represents a column in the list view as you can see in the
          image above.
        </Text>
        <Text>
          Two of the most basic elements needed for a column is{" "}
          <Code>Header</Code> and <Code>accessor</Code>.
        </Text>
        <Text>
          <Code>Header</Code> is the label at the top of the column which you
          can set as anything you want. If you want a column to not have a
          Header at all, you can give <Code>Header</Code> a value of{" "}
          <Code>() =&#62; null</Code>
          .
          <br />
          <Code>accessor</Code> is the key for the value you want to be
          displayed in that column. For example, for the first column called
          'position', we want to display the name of the job. In the data, this
          is under the <Code>name</Code> key, so the accessor must be 'name'.
          You can access any value you want in the data including nested data in
          arrays and/or objects. For example, for the second column we need to
          display the name of the branch. In the data, this lives in an object
          called 'branch' and then under the key of 'name' so to access it, we
          need to give it an accessor of <Code>"branch.name"</Code> as a string.
          <br />
          You can also change the width of each column by using the 'width'
          element. For example <Code>width: '80px'</Code>
        </Text>
        <Text>
          In the boiler plate if you look at the example file for list.jsx you
          will find that the first object in the columns array uses a method
          called 'Cell' as shown below.
        </Text>
        <Flex>
          <Box width="100%">
            <Image src="images/Cell.png" />
          </Box>
          <Box>
            <Image src="images/edit_button.png" />
          </Box>
        </Flex>
        <Text>
          All this does is create a link to the edit view for an entry that
          renders as an edit icon and you won't need to change this very much
          for most uses. Here is an explanation for what this is doing.
          LinkButton is a nucleus component that allows you to link to different
          pages within nucleus by setting the URL to the <Code>to</Code> prop.
          In the to prop, you can see the url is a string literal. The first
          part <Code>setup.model</Code> is the same as the model name that is
          set in setup.js, in this case 'services'. 'edit' will take you to the
          edit view, and <Code>row.original[setup.accessor]</Code> is the id for
          that particular entry. So all of these things combined will give a url
          of: <br />
          "services/edit/19"
          <br />
          where 'services' is the modelname (which is basically the endpoint),
          'edit' is the view, and '19' is the id for the entry that I clicked.
        </Text>
        <Subtitle>Cell</Subtitle>
        <Text>
          You use the cell method for rendering anything more complex than just
          the data returned. For example, you can render a button, or an image
          or the date in a different format.
          <br />
          The Cell method sends two props: <Code>value</Code> and{" "}
          <Code>row</Code>{" "}
        </Text>
        <Subtitle>value</Subtitle>
        <Text>
          The value prop will provide the value for that column. So if you're
          accessor is 'email' then the <Code>value</Code> prop will be the email
          for that user. See below for an example
        </Text>
        <Image src="images/value_example.png" />
        <Subtitle>row</Subtitle>
        <Text>
          Row will allow you to access any data from that entry in the table
          using <Code>row.original</Code>. See below for an example
        </Text>
        <Image src="images/row_example.png" />
        <Subtitle>Search</Subtitle>
        <Text>
          If you set <Code>canSearch</Code> to <Code>true</Code> in setup.js,
          users will be able to search for particular entries in the list view.
          This will enter an input field above the table. When a user uses the
          search bar, nucleus will make another <Code>GET</Code> but this time
          with the search term added as a query. For example:{" "}
          <Code>http://endpoint.stagelab.co.uk/products?search=doritoes</Code>
          It will be up to the backend to make sure the GET request will accept
          a query string.
        </Text>
        <Subtitle>Sort</Subtitle>
        <Text>
          Sorting the data in the list view can be done by clicking on the
          headings. When this is done, nucleus will make a new <Code>GET</Code>
          request appended with a query string. This will include the id value
          for that column, and either 'asc' or 'desc' which you can toggle by
          clicking the heading again.
          <br />
          So if you had a column with the id of "name" then when you click the
          heading, nucleus will make a new GET and add the id as a query string:
          <br />
          <Code>
            http://endpoint.stagelab.co.uk/products?sort=name&#38;order=asc
          </Code>
          <br />
          As with searching, the backend must be setup to accept a parameter for
          sorting and a direction
        </Text>
      </Box>
    </PageContainer>
  );
};

export default List;
