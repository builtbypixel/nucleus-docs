import React from "react";
import { Flex, Box, Text } from "@builtbypixel/plasma";
import PageContainer from "./PageContainer";
import Title from "./styles/Title";
import Subtitle from "./styles/Subtitle";

const GqlIntro = () => {
  return (
    <PageContainer>
      <Title>GraphQL with nucleus</Title>
      <Text>
        Nucleus has now been adapted to work with graphQL queries and has all
        the same CRUD functionality but with queries and mutations instead of
        traditional endpoints. While most things are very similar like the way
        you build up the fields in an edit view, there are a lot of differences
        with how nucleus sends and receives the data.
      </Text>
      <Text mt="15px">
        Before you read this section of the docs, make sure you are aleady
        familiar with the basics of nucleus and GraphQL
      </Text>
    </PageContainer>
  );
};

export default GqlIntro;
