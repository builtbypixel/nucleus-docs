import React from "react";
import { Box, Flex, Text, Image } from "@builtbypixel/plasma";
import Code from "./styles/Code";
import PageContainer from "./PageContainer";
import Title from "./styles/Title";
import Subtitle from "./styles/Subtitle";

const MainRouter = () => {
  return (
    <PageContainer>
      <Box>
        <Title>Main Router</Title>
        <Text>
          In <Code>/src</Code>, there is a file called <Code>routes.js</Code>{" "}
          (not to be confused with the routes.js file inside every container).
          This is the main router of the nucleus project. Everytime you create a
          new container, you must add it to the main router file or the
          container will not be accessible. To add a container to the main
          router, first import the routes file for the container at the top of
          the file. Then spread the routes array into the routes array. See
          below for an example.
        </Text>
        <Box>
          <Image src="images/main_router.png" />
        </Box>
      </Box>
    </PageContainer>
  );
};

export default MainRouter;
