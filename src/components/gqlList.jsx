import React from "react";
import { Flex, Box, Text, Image } from "@builtbypixel/plasma";
import PageContainer from "./PageContainer";
import Title from "./styles/Title";
import Code from "./styles/Code";
import Subtitle from "./styles/Subtitle";

const GqlList = () => {
  return (
    <PageContainer>
      <Title>List view with GraphQL</Title>
      <Text>
        The main difference between the list view in GraphQL and traditional
        nucleus is that all the data fetching takes place in the{" "}
        <Code>List.jsx</Code> file itself rather than happening in the
        background. You will usually be one query and one mutation you will need
        for the list view. The query will get all of the data to populate the
        table and the mutation will allow the user to delete entries by their{" "}
        <Code>_id</Code>. Let's look at one example from Barn Direct (in
        /roles/list.jsx).
      </Text>
      <Flex justifyContent="space-evenly">
        <Image src="/images/gqllist1.png" width="40%" height="40%" />
        <Image src="/images/gqllist2.png" width="40%" height="40%" />
      </Flex>
      <Subtitle>The List Wrapper component</Subtitle>
      <Text>
        When using graphQL, you need to provide the list wrapper with a lot more
        information compared to traditional nucleus. Look at the example below
      </Text>
      <Image src="/images/listwrapper.png" />
      <Text>
        You still have to provide it with the <Code>setup</Code> object and the{" "}
        <Code>columns</Code> array as normal. You also need to give{" "}
        <Code>gqlFetch</Code> the query you use to fetch the data and{" "}
        <Code>gqlDelete</Code> the mutation you use to delete the data. You can
        provide a default sort or filter if you wish as well as an extramenu. If
        you would like to use filters for your table, you need to pass in the
        filters array in here as well.
      </Text>
      <Subtitle>FETCHDATA</Subtitle>
      <Text mt="15px">
        First let's look at the query <Code>FETCHDATA</Code>. This needs to
        return all the data you need to display in the table for the list view.
        Remember- this doesn't have to be all the data that the model can fetch,
        just the most important data that the client can see a summary of. So in
        this example, the data that we want to display comes back in the{" "}
        <Code>items</Code> object in the query string.
        <Text>
          <b>Important-</b> whichever data you are getting back, you must make
          sure you get the id as well as this is how nucleus will link to the
          edit/crete view.
        </Text>
      </Text>
      <Subtitle>DELETE</Subtitle>
      <Text>
        This is the delete mutation that you need to provide in the file. It's
        important to make sure that your mutation expects an array of ids
        instead of just one so the client can delete multiple entries at the
        same time.
      </Text>
      <Subtitle>Pagination</Subtitle>
      <Text>
        In order for the pagination to work in nucleus, you need to make sure
        your query in <Code>FETCHDATA</Code> returns all the page data and also
        accepts page and perPage as arguments (see pageInfo in the example at
        the top). If you are using graphQL composer, this can be provided for
        you. So long as your model in the backend has all the pagination done
        and you build your query to return the pageInfo (like the example),
        nucleus can take that data and implement it into the list view for you.
      </Text>
      <Subtitle>Filters</Subtitle>
      <Text>
        If you want to use filters you need to create an array of objects and
        pass it in to the <Code>ListWrapper</Code>. See below for an of how to
        do this.
      </Text>
      <Image src="/images/filters.png" />
      <Text>
        Here, I am creating a an object in the filters array that will work
        using the value of <Code>isFeatured</Code> in the data. The
        <Code>value</Code> refers to the data the filter will be looking at. The
        type determines the type of filter that will be used. I am using a
        select. Since I am using a select, I need to provide it with an array of
        objects in
        <Code>items</Code> that will create the list of options for the user to
        chose from. Since I know isFeatured is a boolean, I know I will only
        need two values- true and false. I can add these two different options
        as objects in <Code>items</Code>. In each object, the <Code>value</Code>{" "}
        needs to be set to the value in the actual data that will be used to
        filter and the label will be what the user will see in the dropdown menu
        when they are using the filter. Finally the selectValue need to be set
        to 'value', so the filter knows to use the 'value' key.
      </Text>
      <Text mt="15px">
        You also need to make sure that the backend is set up to allow filters
        and the query has <Code>$filter</Code> as an argument.
      </Text>
      <Subtitle>Sorting</Subtitle>
      <Text>
        Providing that the backend has been set up to allow for it, sorting can
        be done on nucleus with graphQL easily. If the user clicks on a heading,
        a sort variable will automatically be passed to the query with the name
        of the key with "_ASC" or "_DESC" appended to it. For example, if you
        click on the top of the column for "price", it will send the query with{" "}
        <Code>sort: "price_ASC"</Code> or (price_DESC) passed into the
        variables. So in order to make that field sortable, the backend needs to
        be setup to accept the values in sort in the variables.
      </Text>
    </PageContainer>
  );
};

export default GqlList;
