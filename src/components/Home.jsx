import React from "react";
import { Box, Text, Flex } from "@builtbypixel/plasma";
import Code from "./styles/Code";
import Title from "./styles/Title";
import Subtitle from "./styles/Subtitle";
import PageContainer from "./PageContainer";

const Home = () => {
  return (
    <PageContainer>
      <Box>
        <Title>Nucleus</Title>
        <Text>
          Welcome to the nucleus docs! You can use this as a guide to build any
          nucleus project using the boiler plate repository and will include
          specific examples both in the code and in the front end. If you are
          building a brand new project, click on Getting Started for everything
          you need to know to get up and running in nucleus.
        </Text>
        <Subtitle>checklist</Subtitle>
        <Text>
          If you're making a new nucleus project, use this checklist to make
          sure you have set everything up that you need to
        </Text>
        <Text>
          <Text as="li">
            Clone the nucleus boiler plate repository and do npm install or yarn
            install
          </Text>
          <Text as="li">
            Create a <Code>.env</Code> file and set it up as specified in
            'Getting Started'
          </Text>
          <Text as="li">
            Create a copy of the container example folder and rename it to an
            endpoint from the backend
          </Text>
          <Text as="li">
            Add your container to the main router of the project (see 'Router')
          </Text>
          <Text as="li">
            Add a link to your new container by adding it to the{" "}
            <Code>links.js</Code> file
          </Text>
          <Text as="li">
            Configure the <Code>Setup.js</Code> file in your container
          </Text>
          <Text as="li">
            Set up the <Code>list.jsx</Code> file in your container (chose what
            data you would like to display in the list view)
          </Text>
          <Text as="li">
            Set up the <Code>index.jsx</Code> file in your container by adding
            fields for each data point in your endpoint in autofields (or manual
            fields)
          </Text>
        </Text>
      </Box>
    </PageContainer>
  );
};

export default Home;
