import React from "react";
import { Flex, Box, Text, Image } from "@builtbypixel/plasma";
import Code from "./styles/Code";
import Title from "./styles/Title";
import Subtitle from "./styles/Subtitle";
import PageContainer from "./PageContainer";

const GqlEdit = () => {
  return (
    <PageContainer>
      <Title>Edit view with GraphQL</Title>
      <Text>
        Edit view is very similar to edit view in traditional nucleus. The main
        difference is that when using graphQL, all of the queries and mutations
        have to be definied in the <Code>index.jsx</Code> file itself. From
        looking at the example below, you can see that there are three main
        queries/mutations that you will need.
      </Text>
      <Image src="/images/gqledit1.png" />
      <Subtitle>FETCH</Subtitle>
      <Text>
        This is the query you will need to fetch a single record by ID. This
        will need to bring back any data that you want to be editable. If you
        don't do this, the data won't be pre-filled when the user wants to edit
        an existing record.
      </Text>
      <Subtitle>EDIT</Subtitle>
      <Text>
        As the name suggests, this is a mutation that should update a single
        entry by the id.
      </Text>
      <Subtitle>CREATE</Subtitle>
      <Text>This is the mutation that you need to create one entry.</Text>
      <Text mt="15px">
        Now let's look at what else we will need for edit view to work.
      </Text>
      <Text mt="15px">
        In the <Code>EditForm</Code> component you will need to use a function
        called <Code>useLazyQuery</Code> which you can import from apollo. This
        function will generate two things. A function that you can use to fetch
        the data (just the query strings will not do this by themselves), and
        the data itself once the funtion you made is called.
      </Text>
      <Text mt="15px">
        You need to pass the FETCH, EDIT and CREATE query, the{" "}
        <Code>fetchData</Code> function that was produced by useLazyQuery, and
        the data that will come back from that query into the{" "}
        <Code>EditView</Code> component. See below for an example of this.
      </Text>
      <Text>
        Then all you have to do is build up the fields using{" "}
        <Code>autoFields</Code> or manual fields just like you normally would
        with traditional nucleus.
      </Text>
      <Image src="/images/gqledit2.png" />
    </PageContainer>
  );
};

export default GqlEdit;
