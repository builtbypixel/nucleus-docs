import React from "react";
import { Flex, Box, Text, Button, Image } from "@builtbypixel/plasma";
import Code from "./styles/Code";

const Components = () => {
  return (
    <Box mx="20px" bg="#eeeeee" p="10px" borderRadius="6px" mt="8px">
      <Flex my="10px">
        <Text width="50%">
          <b>text</b>- this component renders a simple text field that the user
          can type in.
        </Text>
        <Box width="50%">
          <Image src="images/text.png" />
        </Box>
      </Flex>
      <Flex my="10px">
        <Box></Box>
        <Text width="50%">
          <b>textarea</b>- this works in a similar way to a text field but it
          gives the user more space to write text. This should be used whenever
          there is a lot of text that needs to be entered (like a paragraph).
          You can add a <Code>rows</Code> property to alter how large the text
          area will be
        </Text>
        <Box width="50%">
          <Image src="images/textarea.png" />
        </Box>
      </Flex>
      <Flex my="10px">
        <Text width="50%">
          <b>file</b>- this allows the user to upload a file. This is most
          commonly used to upload images but can be used for any file.
          <br />
          You can allow the user to add multiple files by setting the{" "}
          <Code>isMultiple</Code> property and use the <Code>max</Code> property
          to limit how many files the user can upload.
        </Text>
        <Box width="50%">
          <Image src="images/file.png" />
        </Box>
      </Flex>
      <Flex my="10px">
        <Text width="50%">
          <b>select</b>- this component uses the allows users to chose a value
          from a provided list in a dropdown. You must provide it with these
          properties: <br />
          <Code>options</Code>- this is where you put your array that will
          represent the contents of the dropdown (this will always be an array
          of objects). For example:
          <br />
          <Code>
            &#91;&#123;name: "Jim", id: 1&#125;, &#123;name: "Amy", id:
            2&#125;&#93;
          </Code>
          <br />
          <Code>labelKey</Code>- this is the key which will act as the label and
          will be what the user sees in the dropdown. For example{" "}
          <Code>name</Code>
          <br />
          <Code>valueKey</Code>- this is what will be set as the value when the
          form saves. For example <Code>id</Code>.<br />
          <Text fontStyle="italic">note- this will nearly always be "id"</Text>
          <br />
          <Code>isMulti</Code>- this is an optional boolean that will allow
          users to input multiple items from the dropdown.
        </Text>
        <Box width="50%">
          <Image src="images/select.png" />
        </Box>
      </Flex>
      <Flex my="10px">
        <Text width="50%">
          <b>colour</b>- this creates a colour picker which will return a hex
          colour value.
        </Text>
        <Box width="50%">
          <Image src="images/colour.png" />
        </Box>
      </Flex>
      <Flex my="10px">
        <Text width="50%">
          <b>switch</b>- this renders a switch which you click between on and
          off and gives the value of true or false. You can use this to control
          boolean values. For example, whether a product is featured or not
        </Text>
        <Box width="50%">
          <Image src="images/switch.png" />
        </Box>
      </Flex>
      <Box my="10px">
        <Text>
          <b>repeater</b>- Sometimes some of the data comes back as a collection
          of items in an array instead of a single value. For these cases, you
          need to use a repeater component. Once you set the component to{" "}
          <Code>repeater</Code> you must then create a new property called{" "}
          <Code>fieldArray</Code> which will have the value of a new array of
          field objects. Here is an example:
          <br />
          Let's say that in the data for a <Code>products</Code> endpoint, there
          is a <Code>features</Code> key which has the value of an array of
          objects containing all of the features of a product. Each of these
          objects will have the same structure:
          <br />
          <Code>title</Code>
          <br />
          <Code>image</Code>
          <br />
          <Code>description</Code>
          <br />
          This is what you would need to do:
          <br />
          <Text as="li" textDecoration="bullet point">
            Create an autofields object as normal (the light blue brackets).
          </Text>
          <Text as="li" textDecoration="bullet point">
            Add the name property to match the key for the data array. In this
            example, the array comes under the key of <Code>features</Code> so
            the name has to have the same value.
          </Text>
          <Text as="li" textDecoration="bullet point">
            Add a label and any other properties you would like e.g. isRequired.
          </Text>
          <Text as="li" textDecoration="bullet point">
            Add a component property with the value of <Code>repeater</Code>
          </Text>
          <Text as="li" textDecoration="bullet point">
            Add a <Code>fieldArray</Code> property with the value of an empty
            array
          </Text>
          <Text as="li" textDecoration="bullet point">
            In that array, create new field objects (the purple brackets) for
            each data point in the array which in our example is{" "}
            <Code>title</Code>, <Code>image</Code> and <Code>description</Code>.
          </Text>
          Nucleus will then allow the user to add or remove elements to the
          array as well as edit the data inside. Below is an example of how the
          code should look and how this would render in nucleus.
        </Text>
        <Flex>
          <Box width="50%">
            <Image src="images/repeater_code.png" />
          </Box>
          <Box width="50%">
            <Image src="images/repeater_nucleus.png" />
          </Box>
        </Flex>
      </Box>
      <Text>
        For a more detailed description of what components are available and
        what they do, read this confluence article created by Matt Price{" "}
        <Button bg="#d0e9ff">
          <a
            style={{ color: "black" }}
            target="_blank"
            href="https://builtbypixel.atlassian.net/wiki/spaces/PXL/pages/283574273/Input+Fields"
          >
            More Info
          </a>
        </Button>
      </Text>
    </Box>
  );
};

export default Components;
