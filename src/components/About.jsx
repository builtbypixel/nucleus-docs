import React from "react";
import { Box, Text, Flex, Tooltip } from "@builtbypixel/plasma";
import { FiHelpCircle } from "react-icons/fi";
import PageContainer from "./PageContainer";
import Title from "./styles/Title";
import Subtitle from "./styles/Subtitle";

const About = () => {
  return (
    <PageContainer>
      <Box>
        <Title>About</Title>
        <Text>
          Nucleus is a package made by Matt Price from pixel that can be used to
          create an interface that will control some aspects of a site for a
          client. It is made using <Text as="span">React</Text> and{" "}
          <Text as="span">React hook forms</Text>. The key advantage of nucleus
          is that it allows websites made for clients to be customisable.
          Instead of having to contact Pixel to request a change, creating a
          ticket, assigning a dev to it, and then pushing it to live, the client
          can handle most small changes themselves through nucleus.
        </Text>
        <Text>
          Because of the way nucleus is designed, most simple features are done
          for you so you don't have to create the entire form or do any of the
          PUT requests. Nucleus automatically builds a form around the data
          structure and then contacts the backend via a PUT request which will
          then update the frontend/website.
        </Text>
        <Flex alignItems="center">
          <Text mr="5px">
            Nucleus can also create CRUD routes for each endpoint.
          </Text>
          <Tooltip title="Create, Read, Update and Delete">
            <FiHelpCircle />
          </Tooltip>
        </Flex>
        <Subtitle>React hook forms</Subtitle>
        <Text>
          One of the main dependencies nucleus uses is a package called React
          Hook Forms. For some of the more advanced features you will need a
          good understanding of react hook forms. Click{" "}
          <a href="https://react-hook-form.com/">here</a> for the docs.
        </Text>
      </Box>
    </PageContainer>
  );
};

export default About;
