import React from "react";
import { Flex, Box, Text } from "@builtbypixel/plasma";

const PageContainer = ({ children }) => {
  return (
    <Box m="30px" p="20px" bg="#e1e1e1" borderRadius="10px">
      {children}
    </Box>
  );
};

export default PageContainer;
