import React from "react";
import { Box, Flex, Text, Tooltip } from "@builtbypixel/plasma";
import { FiHelpCircle } from "react-icons/fi";
import Title from "./styles/Title";
import PageContainer from "./PageContainer";
import Subtitle from "./styles/Subtitle";

const GettingStarted = () => {
  return (
    <PageContainer>
      <Box>
        <Title>Getting Started</Title>
        <Subtitle width="250px">
          <Flex alignItems="center">
            <Text mr="4px">Downloading the boiler plate</Text>
            <Tooltip title="'boiler plate' means standard code that is always needed for something to work">
              <FiHelpCircle />
            </Tooltip>
          </Flex>
        </Subtitle>
        <Text>
          To start a new nucleus project, you need to download the{" "}
          <a href="https://bitbucket.org/builtbypixel/nucleus-boilerplate/src/master/">
            boiler plate code from bitbucket.
          </a>{" "}
          To do this, follow the link, click the 'clone' button, copy the
          command and then paste the command in your terminal in the folder that
          you want.
        </Text>
        <Subtitle>Installing dependencies</Subtitle>
        <Text>
          Next you will need to open up the repository in VS code. <br />
          After that, make sure you install the dependencies by using 'npm
          install' or 'yarn install' if you have yarn.
        </Text>
        <Subtitle>Creating your .env file</Subtitle>
        <Text mb="10px">
          In the nucleus boilerplate code, you will find a file in the root
          called .env. This file is used for configuring all the settings for a
          nucleus project. It's important to set up your env file otherwise
          nucleus will not run properly.
        </Text>
        <Text>
          Here is what needs to be set for each value stored in your env:
          <br />
          SKIP_PREFLIGHT_CHECK=true
          <br />
          This doesn't need to be changed
          <br />
          REACT_APP_ENVIRONMENT=development
          <br />
          This doesn't need to be changed
          <br />
          REACT_APP_TEST_MODE=true
          <br />
          This doesn't need to be changed
          <br />
          REACT_APP_BYPASS_AUTH=false
          <br />
          <Text>
            By setting this to true, you won't have to log in when you start
            nucleus.
          </Text>
          REACT_APP_API=https://jglapi.stagelab.co.uk
          <Text>
            This is the url for the backend for nucleus to work with. (this can
            be postman if the backend isn't ready yet)
          </Text>
          REACT_APP_API_PREFIX=api
          <br />
          <Text>
            This is the prefic that you may need for the api (backend). for
            example, if all endpoints have the prefix 'api like /api/products,
            then you should define it here.
          </Text>
          REACT_APP_LOGIN_PATH=auth/login
          <br />
          <Text>
            This is the path for the login screen for your nucleus app.
            Generally this doesn't need to be changed
          </Text>
          REACT_APP_FRONTEND_URL=http://jgl.stagelab.co.uk
          <br />
          <Text>
            The url for the frontend project that nucleus will control
          </Text>
          REACT_APP_SITENAME=Site Name
        </Text>
        <Subtitle>Running nucleus</Subtitle>
        <Text>
          to run nucleus locally, just use the command npm start or yarn start
          if you have yarn
        </Text>
      </Box>
    </PageContainer>
  );
};

export default GettingStarted;
