import React, { useState } from "react";
import { Flex, Box, Text, Image, Button } from "@builtbypixel/plasma";
import Components from "./Components";
import Code from "./styles/Code";
import Title from "./styles/Title";
import PageContainer from "./PageContainer";
import Subtitle from "./styles/Subtitle";

const Edit = () => {
  const [list, setList] = useState(false);
  return (
    <PageContainer>
      <Box>
        <Title>index.jsx (edit view)</Title>
        <Text>
          As the name suggests, the edit view allows a user to edit the data
          from the endpoint for the container. The edit view is always in{" "}
          <Code>index.jsx</Code> in the container.
          <br />
          There are two ways of building the edit view in nucleus: auto fields
          and manual fields. Autofields is much easier and has many components
          already built in to speed things up, but only gives you a very minimal
          amount of control. Manual fields is harder and requires you to build
          the front end yourself (including styling) but allows for much more
          control and flexibility.
        </Text>
        <Subtitle>at a glance</Subtitle>
        <Text>
          Here is a simple edit view for an endpoint/model called{" "}
          <Code>/users</Code>.
        </Text>
        <Image src="images/simple_edit.png" />
        <Text>
          If you look at what is being returned from the <Code>EditView</Code>,
          there is an <Code>AutoFields</Code> component wrapped in a component
          called <Code>EditView</Code> both of which are nucleus components.
          <Code>EditView</Code> contains the code that controls the layout of
          the page and does not need to be edited. Autofields is a component
          which uses the 'fields' prop to render a list of field components for
          you.
        </Text>
        <Subtitle>auto fields</Subtitle>
        <Text>
          from line 6, we can see that fields is an array of objects created
          using <Code>useMemo</Code> from react. For each object in this array,
          nucleus will create a field allowing users to change the data for that
          entry.{" "}
          <Text fontStyle="italic">
            Note- autofields will create the fields for you automatically if
            instead, you want to create the fields <b>manually</b> which is
            harder but gives you more control, see the section on{" "}
            <a href="/manual">manual fields</a>
          </Text>
        </Text>
        <Subtitle>groups</Subtitle>
        <Text>
          When using autofields, the fields array MUST belong in at least one
          group. If there are not many fields and you don't need to group the
          fields up, put all the fields in the same group and give the group a
          value of an empty string if you don't need a title. e.g.{" "}
          <Code>
            &#123;group: "", fields: &#91; field objects go in here&#93;
          </Code>
          <br />
          <br /> For larger data structures (for endpoints that return a lot
          data) it's a good idea to divide these sections up into different
          categories in the same edit view. For example, a{" "}
          <Code>/products</Code> endpoint might have: Product details,
          availability, costs etc each of which would each be made up of many
          different fields. To help organise this you can divide the data in{" "}
          <Code>fields</Code> into groups. To do this, create an object in the
          fields array. In that object, ad a key of 'group' and give it the
          value of whatever you want that group to be called. Then add a
          'fields' key which will have the value of an array. Then in that
          array, you can create all of the field objects for that group. <br />
          If you want the different groups to appear on the same page on top of
          each other, then you don't have to do anything else. However, you can
          can also chose to only render one group at a time by giving each group
          a <Code>tab</Code> key. This will create an additional menu to the
          side of the edit view where you can click to view each group of
          fields. The value for <Code>tab</Code> is what will be displayed in
          the menu at the side. See the example below
        </Text>
        <Flex>
          <Box>
            <Image src="images/groups_code.png" />
          </Box>
          <Box>
            <Image src="images/groups_nucleus.png" />
          </Box>
        </Flex>
        <Text>
          As you can see in the example above, each group will appear as a link
          to the left of the edit view that when clicked will render the fields
          for each group in the middle of the screen.
        </Text>
        <Title mt="15px">The auto fields object</Title>
        <Text>
          Once you have your edit view organised into groups (or into one
          unnamed group), you can start creating the field objects themselves.
          Since we are using auto fields we don't have to import or create any
          components ourselves. All we need to do is provide an object with the
          correct information and nucleus will create fields for us.
          <br />
          below is a typical auto field object:
        </Text>
        <Image src="images/auto_field_object.png" />
        <Text>Here is what each property controls</Text>
        <Text>
          <b>name</b>- This must match the key of the value you want to edit in
          the data. In this example, the key would be 'first_name' so the name
          value would have to be exactly that
        </Text>
        <Text>
          <b>component</b>- This determines what component the field will use.
          There are many different components that you can use depending on what
          format the data should be in.
        </Text>
        <Button onClick={() => setList(!list)} bg="#b9d5e4">
          {list ? "Hide all components" : "Show all components"}
        </Button>
        {list && <Components />}
        <Text>
          <b>placeholder</b>- this is what will appear in a text or textarea
          field as a placeholder. It isn't required and doesn't have any effect
          on the actual data
        </Text>
        <Text>
          <b>label</b>- This renders a small title that will appear above the
          field. It doesn't have to be the same as the <Code>name</Code>{" "}
          property. This is just what the user will see and recognise.
        </Text>
        <Text>
          <b>help</b>- This renders a small help message below the field that
          can be used to help the user with information about what the data will
          affect or how to use the field. e.g. "The image you upload will appear
          at the top of the homepage".
        </Text>
        <Text>
          <b>disabled</b>- This is a boolean that if set to true will disable
          the field stopping the user from clicking into the field or entering
          any data.
        </Text>
        <Text>
          <b>defaultValue</b>- This will provide a default value for the field.
          NOTE- most of the time this won't be needed as the field should
          already be pre-populated with the vale from the <Code>name</Code> key
          which will override the default value. However, if you know the data
          will typically come back as empty or null, a defaultValue can be set
        </Text>
        <Text>
          <b>singleName</b>- if you are using the repeater you can use this
          property to change the text on the button to add new items instead of
          just displaying 'add new item'
        </Text>
        <Text>
          <b>editOnly</b>- this is a boolean that if set to true, will only
          allow the field to be rendered when in the edit view. It will not
          appear when you try and create a new entry
        </Text>
        <Text>
          <b>createOnly</b>- this is a boolean that if set to true, will only
          allow the field to be rendered when creating a new entry. It will not
          appear in edit view.
        </Text>
        <Text>
          <b>isRequired</b>- This should be set to true if you want that field
          to be required. You will also need{" "}
          <Code>rules: &#123;required: true&#125;</Code>
        </Text>
        <Text>
          <b>rules</b>- This adds validation rules from React Hook Forms. It
          must be given the value of an object with the type of rule as the key.
        </Text>
        <Text as="li">
          required- setting this to true will make the field a required field.
          Example- <Code>rules: &#123;required: true&#125;</Code>
        </Text>
        <Text as="li">
          min- sets the minimum value for a number (input type must be a number,
          not a string). Example- <Code>rules: &#123;min: 8&#125;</Code>
        </Text>
        <Text as="li">
          max- sets the maximum value for a number (input type must be a number,
          not a string). Example- <Code>rules: &#123;max: 100&#125;</Code>
        </Text>
        <Text as="li">
          minLength- sets the minimum length for a string input. Example-{" "}
          <Code>rules: &#123;minLength: 5&#125;</Code>
        </Text>
        <Text as="li">
          maxLength- sets the maximum length for a string input. Example-{" "}
          <Code>rules: &#123;maxLength: 50&#125;</Code>
        </Text>
        <Text as="li">
          pattern- when provided a RegEx expression, will test to see if the
          input matches. Example-{" "}
          <Code>rules: &#123;pattern: /\w+@\w+.\w+/&#125;</Code> will match any
          valid email address
        </Text>
        <Text fontStyle="italic">
          Note- you can also add your own validation schema from another library
          e.g. Yup
        </Text>
        <Subtitle>Custom components</Subtitle>
        <Text>
          As well as the components that comes with nucleus out of the box, you
          can also create and use your own custom components. For more detail on
          how to do this, read the section on{" "}
          <a href="/custom">custom components</a>. However, if all you want to
          do is create a simple component such as creating a subtitle or a
          divider, you can do this very easily by creating an autofield object
          with an empty string for a name and a normal React functional
          component. See below for an example:
        </Text>
        <Image src="images/simple_custom.png" />
        <Text>
          This is a quick and easy way to give an extra level of detail and
          control to the edit view even when using autofields.
        </Text>
      </Box>
    </PageContainer>
  );
};

export default Edit;
